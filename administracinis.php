<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device, initial-scale=1.0">
<title>Document</title>
</head>
<body>
    <form method="get" action="./pridejimas-sarasas.php">
    <input type="text" name="pavadinimas"/>
    <input type="number" name="kaina" step="0.01" value="0"/>
    <input type="submit"/>
   </form>
   </form>
    <h2> Sąrašas</h2>
    <?php 
        //Noru sarašo spausdinimui
        if(file_exists('./prekiu-sarasas.json')) {
            if(is_readable('./prekiu-sarasas.json')) {
                $contents = file_get_contents('./prekiu-sarasas.json');
                $array = json_decode($contents, true);
 
                if (isset($_GET['delid'])) {
                    unset($array[$_GET['delid']]);
                    $iJSON = json_encode($array, true, JSON_PRETTY_PRINT);
                    file_put_contents('./prekiu-sarasas.json', $iJSON);
                }
            }
        } 
             
    ?>

<table border="1px" cellpadding="2px">
        <tr>
            <th>Pavadinimas</th>
            <th>Kaina</th>
            <th>Šalinti</th>
            <th>Redagavimas</th>
            
            
        </tr>
        <?php if(!empty($array))foreach($array as $key => $value) : ?>
            <tr>
                <td><?php echo $value['preke']?></td>
                <td><?php echo $value['kaina']?></td>
                <td><a href=<?php echo "\"?delid=" . $key . "\""; ?>>Istrinti</a></td>
                <td><a href="./redagavimo-sarasas.php?">Redaguoti</a></td>
            </tr>
            <?php endforeach;
            else { ?>
                <td colspan="3"><?php echo "Sąrašas yra tuščias" ?></td>


            <?php } ?>
 
    </table>
  </body>
</html>