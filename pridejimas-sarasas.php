<?php
if (isset($_GET['pavadinimas']) && isset($_GET['kaina'])) {
    if(file_exists('./prekiu-sarasas.json')){
        if(is_writable('./prekiu-sarasas.json')){
            $contents = file_get_contents('./prekiu-sarasas.json');
            $elementai = json_decode($contents, JSON_OBJECT_AS_ARRAY);
            array_push(
                $elementai,
                [
                    "preke" => trim($_GET['pavadinimas']),
                    "kaina" => trim($_GET['kaina'])
                ]
            );
        }
    } else {
        $elementai = [
            [
                "preke" => trim($_GET['pavadinimas']),
                "kaina" => trim($_GET['kaina'])
            ]
        ];
    }
    if(is_writable('./prekiu-sarasas.json')
        || !file_exists('./prekiu-sarasas.json')){
        $ifaila = json_encode($elementai);
        file_put_contents('./prekiu-sarasas.json', $ifaila);
    }
}
header("Location: ./administracinis.php");
